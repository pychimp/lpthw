#!/usr/bin/env python3


name = 'Pratheek'
age = 19
height = 72  # inches
weight = 75
eyes = 'Black'
teeth = 'White'
hair = 'Black'

print("Let's talk about {}".format(name))
print("He's about {}".format(height))
print("He's {} kgs heavy".format(weight))
print("He's got {0} eyes and {1} hair".format(eyes, hair))
print("His teeth are usually {} depending on coffee".format(teeth))
